using System.Diagnostics;
using System.Linq;

namespace Sudoku_solver_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            const int sudokuSize = 3;
            const int sudokuSize2 = sudokuSize * sudokuSize;
            const int width = 625;
            const int height = 625;
            const int xOffset = 10;
            const int panelYOffset = 50;
            const int buttonYOffset = 10;
            const int buttonWidth = 100;
            const int buttonHeight = 35;
            var cellFont = new Font("Courier New", 35, FontStyle.Regular);
            var hintFont = new Font("Courier New", 10, FontStyle.Regular);

            Panel panel1 = new()
            {
                Dock = DockStyle.Fill,
                Location = new Point(0, 0),
                Margin = new Padding(3, 3, 3, 3),
                Name = "panel1",
                TabIndex = 1
            };

            Button updateButton = new()
            {
                Location = new Point(xOffset, buttonYOffset),
                Size = new Size(buttonWidth, buttonHeight),
                Text = "Update",
            };

            Button backtrackButton = new()
            {
                Location = new Point(535, buttonYOffset),
                Size = new Size(buttonWidth, buttonHeight),
                Text = "Backtrack",
            };

            panel1.Controls.Add(updateButton);
            panel1.Controls.Add(backtrackButton);

            TextBox[][] textBoxes = new TextBox[sudokuSize2][];

            for (int i = 0; i < sudokuSize2; i++)
            {
                textBoxes[i] = new TextBox[sudokuSize2];

                for(int j = 0; j < sudokuSize2; j++)
                {
                    const int offset = 70;
                    const int textBoxSize = 65;

                    TextBox t = textBoxes[i][j] = new()
                    {
                        Location = new Point(xOffset + offset * i, panelYOffset + offset * j),
                        Size = new Size(textBoxSize, textBoxSize),
                        Multiline = true,
                        Font = cellFont,
                        TextAlign = HorizontalAlignment.Center,
                    };

                    panel1.Controls.Add(t);
                }
            }

            for(int j = 0; j < sudokuSize - 1; j++)
            {
                const int offset = 207;
                const int multiplier = 210;
                const int labelSize = 2;

                Label columnLine = new()
                {
                    BorderStyle = BorderStyle.FixedSingle,
                    AutoSize = false,
                    Location = new Point(xOffset + offset + j * multiplier, panelYOffset),
                    Size = new Size(labelSize, height),
                };

                Label rowLine = new()
                {
                    BorderStyle = BorderStyle.FixedSingle,
                    AutoSize = false,
                    Location = new Point(xOffset, panelYOffset + offset + j * multiplier),
                    Size = new Size(width, labelSize),
                };

                panel1.Controls.Add(rowLine);
                panel1.Controls.Add(columnLine);
            }

            Controls.Add(panel1);

            CellGrid cellGrid = new(sudokuSize);

            //TODO parsov�n� sudoku gridu
            /*
            textBoxes[0][1].Text = "2";
            textBoxes[1][0].Text = "9";
            textBoxes[1][3].Text = "4";
            textBoxes[1][7].Text = "5";
            textBoxes[2][3].Text = "2";
            textBoxes[2][4].Text = "7";
            textBoxes[2][8].Text = "4";
            textBoxes[3][2].Text = "3";
            textBoxes[3][5].Text = "4";
            textBoxes[4][0].Text = "2";
            textBoxes[4][1].Text = "6";
            textBoxes[4][3].Text = "5";
            textBoxes[4][7].Text = "3";
            textBoxes[5][2].Text = "8";
            textBoxes[5][6].Text = "6";
            textBoxes[6][0].Text = "5";
            textBoxes[6][3].Text = "7";
            textBoxes[6][7].Text = "9";
            textBoxes[6][8].Text = "6";
            textBoxes[7][4].Text = "6";
            textBoxes[7][5].Text = "1";
            textBoxes[8][0].Text = "4";
            textBoxes[8][4].Text = "2";
            textBoxes[8][8].Text = "3";
            */
            textBoxes[0][0].Text = "8";
            textBoxes[1][2].Text = "7";
            textBoxes[1][3].Text = "5";
            textBoxes[1][8].Text = "9";
            textBoxes[2][1].Text = "3";
            textBoxes[2][6].Text = "1";
            textBoxes[2][7].Text = "8";
            textBoxes[3][1].Text = "6";
            textBoxes[3][5].Text = "1";
            textBoxes[3][7].Text = "5";
            textBoxes[4][2].Text = "9";
            textBoxes[4][4].Text = "4";
            textBoxes[5][3].Text = "7";
            textBoxes[5][4].Text = "5";
            textBoxes[6][2].Text = "2";
            textBoxes[6][4].Text = "7";
            textBoxes[6][8].Text = "4";
            textBoxes[7][5].Text = "3";
            textBoxes[7][6].Text = "6";
            textBoxes[7][7].Text = "1";
            textBoxes[8][6].Text = "8";

            updateButton.Click += new EventHandler((object? sender, EventArgs e) =>
            {
                Cell[][] cells = UpdateGrid();
                cellGrid.UpdateValues();
                UpdateTextBoxes(cells);
            });

            backtrackButton.Click += new EventHandler((object? sender, EventArgs e) =>
            {
                UpdateGrid();
                UpdateTextBoxes(cellGrid.Backtrack());
            });

            Cell[][] UpdateGrid()
            {
                return cellGrid.UpdateGrid(textBoxes.Select(row => row.Select<TextBox, int?>(cell => cell.Text.Length == 1 ? int.Parse(cell.Text) : null).ToArray()).ToArray());
            }

            void UpdateTextBoxes(Cell[][] cells)
            {
                //TODO povolit ps�t jenom ��sla men�� ne� sudokuSize2 do textbox�
                for(int i = 0; i < sudokuSize2; i++)
                {
                    for(int j = 0; j < sudokuSize2; j++)
                    {
                        var cell = cells[i][j];
                        var value = cell.Value;
                        var textBox = textBoxes[i][j];

                        if(value == null)
                        {
                            string text = "";
                            for(int k = 0; k < sudokuSize2; k++)
                            {
                                text += (cell.posibilities[k] ? (k + 1) : " ") + " ";
                                if(k % 3 == 2)
                                {
                                    text += Environment.NewLine;
                                }
                            }
                            textBox.Font = hintFont;
                            textBox.Text = text;
                        }
                        else
                        {
                            textBox.Font = cellFont;
                            textBox.Text = value.ToString();
                        }
                    }
                }
            }
        }
    }
}
