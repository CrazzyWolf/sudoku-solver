﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static Sudoku_solver_2.CellGrid;

namespace Sudoku_solver_2
{
    internal class CellGrid: ICloneable
    {
        readonly int size;
        readonly int size2;
        private Cell[][] cells;

        public CellGrid(int size)
        {
            this.size = size;
            size2 = size * size;
            cells = new Cell[size2][];

            for(int i = 0; i < size2; i++)
            {
                cells[i] = new Cell[size2];
                for(int j = 0; j < size2; j++)
                {
                    cells[i][j] = new Cell(size);
                }
            }
        }

        public Cell[][] UpdateGrid(int?[][] values)
        {
            for(int i = 0; i < size2; i++)
            {
                for(int j = 0; j < size2; j++)
                {
                    cells[i][j].Value = values[i][j];
                }
            }

            return cells;
        }

        public void UpdateValues() //TODO rozdělit do funkcí
        {
            bool change = true;
            while(change)
            {
                change = false;

                for(int i = 0; i < size2; i++)
                {
                    for(int j = 0; j < size2; j++)
                    {
                        change |= TestCell(i, j);
                    }
                }

                //pro každou possibility v bloku, pokud tvoří řádek/sloupec, tak odstranit možnosti v ostatních blokách řádku/sloupce
                for(int iOffset = 0; iOffset < size2; iOffset += size)
                {
                    int iOffsetMax = iOffset + size - 1;
                    for(int jOffset = 0; jOffset < size2; jOffset += size)
                    {
                        int jOffsetMax = jOffset + size - 1;
                        for(int index = 0; index < size2; index++)
                        {
                            HashSet<int> xValues = [];
                            HashSet<int> yValues = [];

                            for(int x = iOffset; x <= iOffsetMax; x++)
                            {
                                for(int y = jOffset; y <= jOffsetMax; y++)
                                {
                                    if(cells[x][y].posibilities[index])
                                    {
                                        xValues.Add(x);
                                        yValues.Add(y);
                                    }
                                }
                            }

                            bool oneX = xValues.Count == 1;
                            bool oneY = yValues.Count == 1;

                            if(oneX && oneY)
                            {
                                continue; //value už je známo
                            }

                            if(oneX)
                            {
                                int i = xValues.First();
                                for(int j = 0; j < size2; j++)
                                {
                                    if((j < jOffset || j > jOffsetMax) && cells[i][j].posibilities[index])
                                    {
                                        change = true;
                                        cells[i][j].RemovePosibility(index);
                                    }
                                }
                            }

                            if(oneY)
                            {
                                int j = yValues.First();
                                for(int i = 0; i < size2; i++)
                                {
                                    if((i < iOffset || i > iOffsetMax) && cells[i][j].posibilities[index])
                                    {
                                        change = true;
                                        cells[i][j].RemovePosibility(index);
                                    }
                                }
                            }
                        }
                    }
                }

                //TOOD přidat další strategie

                for(int i = 0; i < size2; i++)
                {
                    for(int j = 0; j < size2; j++)
                    {
                        if(cells[i][j].RefreshValue()) //pokud 1 možnost tak true
                        {
                            change = true;
                        }
                    }
                }
            }
        }

        private bool TestCell(int i, int j)
        {
            bool change = false;

            Cell cell = cells[i][j];
            int? value = cell.Value;

            if(value == null)
            {
                for(int index = 0; index < size2; index++)
                {
                    if(cell.posibilities[index]) //pouze jedna možnost = value
                    {
                        bool row = true;
                        bool col = true;
                        bool block = true;

                        for(int k = 0; k < size2; k++)
                        {
                            if(j != k && cells[i][k].posibilities[index]) //řádky
                            {
                                row = false;
                                break;
                            }
                        }

                        for(int k = 0; k < size2; k++)
                        {
                            if(i != k && cells[k][j].posibilities[index]) //sloupce
                            {
                                col = false;
                                break;
                            }
                        }

                        int iOffset = i / size * size;
                        int jOffset = j / size * size;
                        for(int k = iOffset; k < iOffset + size; k++)
                        {
                            for(int l = jOffset; l < jOffset + size; l++)
                            {
                                if((i != k || j != l) && cells[k][l].posibilities[index]) //bloky
                                {
                                    block = false;
                                    break;
                                }
                            }
                            if(!block)
                            {
                                break;
                            }
                        }

                        if(row || col || block)
                        {
                            change = true;
                            cell.Value = index + 1;
                            break;
                        }
                    }
                }
            }
            else
            {
                int index = (int)(value - 1);
                for(int k = 0; k < size2; k++) //řádky a sloupce
                {
                    if(k != j)
                    {
                        cells[i][k].RemovePosibility(index);
                    }
                    if(k != i)
                    {
                        cells[k][j].RemovePosibility(index);
                    }
                }

                int iOffset = i / size * size;
                int jOffset = j / size * size;

                for(int k = iOffset; k < size + iOffset; k++) //bloky
                {
                    for(int l = jOffset; l < size + jOffset; l++)
                    {
                        if(k != i || l != j)
                        {
                            cells[k][l].RemovePosibility(index);
                        }
                    }
                }
            }

            return change;
        }

        public object Clone()
        {
            CellGrid cellGrid = new(size);
            cellGrid.UpdateGrid(cells.Select(row => row.Select(cell => cell.Value).ToArray()).ToArray());
            return cellGrid;
        }

        public Cell[][] Backtrack()
        {
            UpdateValues();
            List<(int i, int j, int index)> possibilities = [];

            for(int i = 0; i < size2; i++)
            {
                for(int j = 0; j < size2; j++)
                {
                    if(cells[i][j].Value == null)
                    {
                        for(int index = 0; index < size2; index++)
                        {
                            if(cells[i][j].posibilities[index])
                            {
                                possibilities.Add((i, j, index));
                            }
                        }
                    }
                }
            }

            //typnutí jednoho čísla
            while(true)
            {
                var possibilitiesCopy = possibilities.ToList();

                foreach(var possibility in possibilitiesCopy) //hádání value
                {
                    (int i, int j, int index) = possibility;

                    var copy = (CellGrid)Clone();
                    copy.cells[i][j].Value = index + 1;
                    copy.UpdateValues();

                    var result = copy.CheckGrid();

                    if(result == Result.Wrong) //nemůže být tato value
                    {
                        cells[i][j].RemovePosibility(index);
                        possibilities.Remove(possibility);
                    }
                    else if(result == Result.Done) //grid je celý správně vyplněn
                    {
                        return cells = copy.cells;
                    }
                }

                if(possibilities.Count == possibilitiesCopy.Count) //nebyly odstraněny žádné possibility
                {
                    break;
                }
            }

            //pokud typnout jedno číslo nestačilo
            List<(int i, int j, int index, int i2, int j2, int index2)> twoGuesses = [];
            foreach((int i, int j, int index) in possibilities)
            {
                foreach((int i2, int j2, int index2) in possibilities)
                {
                    if(i != i2 || j != j2)
                    {
                        twoGuesses.Add((i, j, index, i2, j2, index2));
                    }
                }
            }

            //typnutí dvou čísel
            foreach((int i, int j, int index, int i2, int j2, int index2) in twoGuesses)
            {
                var copy = (CellGrid)Clone();
                copy.cells[i][j].Value = index + 1;
                copy.cells[i2][j2].Value = index2 + 1;
                copy.UpdateValues();
                
                if(copy.CheckGrid() == Result.Done) //grid je celý správně vyplněn, nelze odstranit possibility při Result.Wrong
                {
                    return cells = copy.cells;
                }
            }

            return cells;
        }

        public enum Result
        {
            Wrong,
            Unknown,
            Done,
        }

        public Result CheckGrid()
        {
            for(int iOffset = 0; iOffset < size2; iOffset += 3) //kontrola bloků a value je někde null
            {
                for(int jOffset = 0; jOffset < size2; jOffset += 3)
                {
                    HashSet<int> numbers = [];

                    for(int i = iOffset; i < iOffset + size; i++)
                    {
                        for(int j = jOffset; j < jOffset + size; j++)
                        {
                            var cell = cells[i][j];
                            var value = cell.Value;
                            if(value == null)
                            {
                                int count = 0;

                                for(int k = 0; k < size2; k++)
                                {
                                    if(cell.posibilities[k])
                                    {
                                        count++;
                                    }
                                }

                                //0 možností znamená chybu, 1 nenastane, protože by value nebylo null a pokud více jak 1, tak není neznámo
                                return count == 0 ? Result.Wrong : Result.Unknown;
                            }
                            else if(!numbers.Add((int)value)) //pokud číslo již existuje v bloku
                            {
                                return Result.Wrong;
                            }
                        }
                    }
                }
            }

            for(int i = 0; i < size2; i++) //kontrola jestli se číslo opakuje ve sloupci nebo řádku
            {
                HashSet<int> rowNumbers = [];
                HashSet<int> colNumbers = [];
                for(int j = 0; j < size2; j++)
                {
                    if(!rowNumbers.Add(cells[i][j].Value ?? throw new Exception()) || !colNumbers.Add(cells[j][i].Value ?? throw new Exception()))
                    {
                        return Result.Wrong;
                    }
                }
            }

            return Result.Done;
        }
    }
}
