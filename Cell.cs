﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku_solver_2
{
    internal class Cell
    {
        //readonly int size;
        readonly int size2;
        public readonly bool[] posibilities;
        private int? value = null;
        public int? Value
        {
            get { return value; }
            set
            {
                this.value = value;
                if(value == null)
                {
                    for(int i = 0; i < size2; i++)
                    {
                        posibilities[i] = true;
                    }
                }
                else
                {
                    for(int i = 0; i < size2; i++)
                    {
                        posibilities[i] = false;
                    }

                    posibilities[(int)(value - 1)] = true;
                }
            }
        }

        public Cell(int size)
        {
            //this.size = size;
            size2 = size * size;
            posibilities = new bool[size2];

            for(int i = 0; i < size2; i++)
            {
                posibilities[i] = true;
            }
        }

        public void RemovePosibility(int index)
        {
            posibilities[index] = false;
        }

        public bool RefreshValue()
        {
            if(value != null)
            {
                return false;
            }

            List<int> p = [];

            for(int i = 0; i < size2; i++)
            {
                if(posibilities[i])
                {
                    p.Add(i);
                }
            }

            if(p.Count == 1) //pouze 1 možnost
            {
                value = p[0] + 1;
                return true;
            }
            return false;
        }
    }
}
